package com.logikujo.testAlate

import models._

import scalaz._
import Scalaz._

import org.scalatra._
import scalate.ScalateSupport

import com.novus.salat.global._
import com.novus.salat._
import com.mongodb.casbah.Imports._

import net.liftweb.json._
import net.liftweb.json.JsonDSL._

trait JSONResultSupport {
  def success = ("status" -> "success") ~ ("results" -> List())
  def success(v:JValue) = ("status" -> "success") ~ ("results" -> v)
  def failure = ("status" -> "failure") ~ ("results" -> List())
}

class TestAlateServlet 
  extends ScalatraServlet 
  with ScalateSupport
  with JSONResultSupport 
{
  val defaultAttributes = Map(
    "title" -> "TestAlate Contacts Manager"
  )

  before() {
    // Use with scal
    // templateAttributes("user") = user
  }

  get("/") {
    contentType = "text/html"

    templateEngine.layout("index.scaml", defaultAttributes + ("test" -> "test"))
  }

  get("/contacts") {
//    contentType = "text/json"
    val json:JValue = try {
      ("status" -> "success") ~
      ("results" -> 
        (grater[ContactList].toJSON(ContactList(ContactDAO.findToList)) \\
          "cList" remove ( _ match {
            case JField("_typeHint", a) => true
            case _ => false
            })))
    } catch {
      case e => ("status" -> "failure") ~ ("results" -> List())
    }
    compact(render(json))
  }

  get("/add/:name/:surname") {
    val json:JValue = try {
      ContactDAO.save(Contact(name = params("name"), surname = params("surname")))
      success
      //("status" -> "success")
    } catch {
      case e => failure
      //("status" -> "failure") ~ ("results" -> List())
    }
    compact(render(json))
  }

  get("/delete/:id") {
    val json:JValue = try {
      ContactDAO.removeById(new ObjectId(params("id")))
      ("status" -> "success")
    } catch {
      case e => failure
     // case e => ("status" -> "failure")
    }
    compact(render(json))
  }

  get("/update/:id/:name") {
    val json:JValue = try {
      val oc = ContactDAO.findOneByID(new ObjectId(params("id")))
      oc.some((c) => {
        ContactDAO.save(c.copy(name = params("name")))
        ("status" -> "success")
      }).none(("status" -> "failure"))
    } catch {
      case e => ("status" -> "failure")
    }
    compact(render(json))
  }

  get("/update/:id/:name/:surname") {
    val json:JValue = try {
      val oc = ContactDAO.findOneByID(new ObjectId(params("id")))
      oc.some((c) => {
        ContactDAO.save(c.copy(name = params("name"), surname = params("surname")))
        ("status" -> "success")
      }).none("status" -> "failure")
    } catch {
      case e => ("status" -> "failure")
    }
    compact(render(json))
  }

  notFound {
    // Try to render a ScalateTemplate if no route matched
    findTemplate(requestPath) map { path =>
      contentType = "text/html"
       layoutTemplate(path, defaultAttributes.toSeq: _*)
    } orElse serveStaticResource() getOrElse {
      contentType = "text/html"
      halt(404, "<h1>Always look at the bright side of life</h1>")
    }
    //resourceNotFound() 
  }
}
