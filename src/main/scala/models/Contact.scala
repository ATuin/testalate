package com.logikujo.testAlate.models

import com.novus.salat.annotations._
import com.mongodb.casbah.Imports._
import com.novus.salat.global._
import com.novus.salat._
import com.novus.salat.dao.SalatDAO

case class Contact (
  @Key("_id")
    id:ObjectId = new ObjectId,
  name:String,
  surname:String)

object ContactDAO extends SalatDAO[Contact, ObjectId](
  collection = MongoConnection()("testalate")("contacts")) {

  def findToList(o:MongoDBObject) = find(o).toList
  def findToList = find(MongoDBObject()).toList
}

case class ContactList (cList: List[Contact])

