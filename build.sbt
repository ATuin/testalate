organization := "com.logikujo"

name := "testAlate"

version := "0.1.0-SNAPSHOT"

scalaVersion := "2.9.1"

seq(webSettings :_*)

libraryDependencies ++= Seq(
  "org.scalatra" % "scalatra" % "2.1.0-SNAPSHOT",
  "org.scalatra" % "scalatra-scalate" % "2.1.0-SNAPSHOT",
  "org.scalatra" % "scalatra-specs2" % "2.1.0-SNAPSHOT" % "test",
  "ch.qos.logback" % "logback-classic" % "1.0.0" % "runtime",
  "org.eclipse.jetty" % "jetty-webapp" % "7.6.0.v20120127" % "container",
  "javax.servlet" % "javax.servlet-api" % "3.0.1" % "provided",
  "org.scalaz" %% "scalaz-core" % "6.0.4",
  "com.novus" % "salat-core_2.9.1" % "0.0.8-SNAPSHOT"
)

resolvers += "Novus Snapshots Repository" at "http://repo.novus.com/snapshots/"

resolvers += "Sonatype OSS Snapshots" at "http://oss.sonatype.org/content/repositories/snapshots/"

resolvers += "Testing" at "http://scala-tools.org/repo-releases/"

// CofeeScripted Plugin Configuration

seq(coffeeSettings :_*)

resourceManaged in (Compile, CoffeeKeys.coffee) <<=
  sourceDirectory(_ / "main" / "webapp" / "public" / "js")


